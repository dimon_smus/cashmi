# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150928163036) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "currencies", primary_key: "currency_id", force: :cascade do |t|
    t.string "currency_symbol", limit: 3,   null: false
    t.string "currency_name",   limit: 255, null: false
  end

  create_table "devices", primary_key: "device_id", force: :cascade do |t|
    t.integer  "device_type",  limit: 4,   null: false
    t.string   "device_token", limit: 255, null: false
    t.integer  "user_id",      limit: 4,   null: false
    t.datetime "updated_at",               null: false
  end

  add_index "devices", ["user_id"], name: "user_id", using: :btree

  create_table "favourites", primary_key: "favourite_id", force: :cascade do |t|
    t.integer  "user_id",             limit: 4, null: false
    t.integer  "friend_id",           limit: 4, null: false
    t.integer  "loan_id",             limit: 4, null: false
    t.datetime "updated_at",                    null: false
    t.integer  "loan_application_id", limit: 4
  end

  add_index "favourites", ["friend_id"], name: "friend_id", using: :btree
  add_index "favourites", ["loan_application_id"], name: "loan_application_id", using: :btree
  add_index "favourites", ["loan_id"], name: "loan_id", using: :btree
  add_index "favourites", ["user_id"], name: "user_id", using: :btree

  create_table "i_friends", force: :cascade do |t|
    t.integer  "user_id",             limit: 4,                  null: false
    t.integer  "friend_id",           limit: 4,                  null: false
    t.integer  "loan_id",             limit: 4,                  null: false
    t.integer  "type",                limit: 4,    default: 0,   null: false
    t.string   "message",             limit: 1000
    t.integer  "who",                 limit: 4
    t.datetime "time",                                           null: false
    t.string   "status",              limit: 2,    default: "0"
    t.datetime "delivery_time",                                  null: false
    t.integer  "is_read",             limit: 4,    default: 0
    t.integer  "loan_application_id", limit: 4
    t.integer  "sent_time",           limit: 4
  end

  add_index "i_friends", ["friend_id"], name: "friend_id", using: :btree
  add_index "i_friends", ["loan_application_id"], name: "loan_application_id", using: :btree
  add_index "i_friends", ["loan_id"], name: "loan_id", using: :btree
  add_index "i_friends", ["user_id"], name: "user_id", using: :btree

  create_table "loan_applications", primary_key: "loan_application_id", force: :cascade do |t|
    t.integer "loan_id",                 limit: 4,                null: false
    t.integer "lender_id",               limit: 4,                null: false
    t.integer "loan_application_status", limit: 4,  default: 0,   null: false
    t.float   "return_amount",           limit: 24, default: 0.0, null: false
    t.integer "per",                     limit: 4,  default: 0,   null: false
    t.integer "over_times",              limit: 4,  default: 0,   null: false
    t.integer "sent_time",               limit: 4,  default: 0,   null: false
    t.integer "bidded_time",             limit: 4,  default: 0,   null: false
    t.integer "confirmed_time",          limit: 4,  default: 0,   null: false
  end

  add_index "loan_applications", ["lender_id"], name: "lender_id", using: :btree
  add_index "loan_applications", ["loan_id"], name: "loan_id", using: :btree

  create_table "loans", primary_key: "loan_id", force: :cascade do |t|
    t.integer "borrower_id",                   limit: 4,                 null: false
    t.float   "amount",                        limit: 24,  default: 0.0, null: false
    t.integer "time",                          limit: 4,   default: 0,   null: false
    t.integer "status",                        limit: 4,   default: 0,   null: false
    t.integer "confirmed_loan_application_id", limit: 4,   default: 0,   null: false
    t.string  "aip",                           limit: 255, default: "0"
  end

  add_index "loans", ["borrower_id"], name: "borrower_id", using: :btree

  create_table "logins", force: :cascade do |t|
    t.string   "auth_token", limit: 100, null: false
    t.integer  "user_id",    limit: 4,   null: false
    t.string   "device_id",  limit: 100
    t.datetime "updated_at",             null: false
  end

  add_index "logins", ["device_id"], name: "device_id", using: :btree
  add_index "logins", ["user_id"], name: "user_id", using: :btree

  create_table "notifications", primary_key: "notification_id", force: :cascade do |t|
    t.integer "loan_application_id", limit: 4
    t.integer "loan_id",             limit: 4,               null: false
    t.integer "loan_status",         limit: 4
    t.integer "user_id",             limit: 4,               null: false
    t.integer "receive_id",          limit: 4
    t.string  "user_name",           limit: 255
    t.string  "photo",               limit: 255
    t.string  "message",             limit: 255,             null: false
    t.integer "time",                limit: 4,               null: false
    t.integer "is_read",             limit: 4,   default: 0
    t.integer "type",                limit: 4
  end

  add_index "notifications", ["loan_application_id"], name: "loan_application_id", using: :btree

  create_table "privacies", force: :cascade do |t|
    t.text "privacy_text", limit: 65535, null: false
  end

  create_table "rates", primary_key: "rate_id", force: :cascade do |t|
    t.integer "loan_id",       limit: 4,    null: false
    t.integer "from_user_id",  limit: 4,    null: false
    t.integer "to_user_id",    limit: 4,    null: false
    t.float   "rate",          limit: 24,   null: false
    t.string  "rate_text",     limit: 1000, null: false
    t.integer "rating_time",   limit: 8,    null: false
    t.integer "rating_status", limit: 4,    null: false
  end

  create_table "settings", primary_key: "setting_id", force: :cascade do |t|
    t.string  "setting_key", limit: 100, null: false
    t.integer "setting_int", limit: 4,   null: false
  end

  create_table "terms", force: :cascade do |t|
    t.text "terms_text", limit: 65535, null: false
  end

  create_table "ticket_messages", primary_key: "ticket_message_id", force: :cascade do |t|
    t.integer "ticket_id",     limit: 4,     null: false
    t.integer "is_from_admin", limit: 4,     null: false
    t.string  "admin_name",    limit: 255,   null: false
    t.text    "message",       limit: 65535, null: false
    t.string  "time",          limit: 30,    null: false
  end

  create_table "tickets", primary_key: "ticket_id", force: :cascade do |t|
    t.string  "subject",       limit: 500, null: false
    t.integer "user_id",       limit: 4,   null: false
    t.string  "created_time",  limit: 100, null: false
    t.integer "ticket_status", limit: 4,   null: false
  end

  create_table "users", primary_key: "user_id", force: :cascade do |t|
    t.integer "is_borrower",      limit: 4,   default: 0,   null: false
    t.integer "is_staff",         limit: 4,   default: 0,   null: false
    t.string  "photo",            limit: 255, default: "",  null: false
    t.string  "name",             limit: 255, default: "",  null: false
    t.string  "nric",             limit: 255, default: "",  null: false
    t.string  "password",         limit: 255, default: "",  null: false
    t.string  "email",            limit: 255, default: "",  null: false
    t.string  "phone",            limit: 255, default: "",  null: false
    t.string  "job",              limit: 255, default: "",  null: false
    t.integer "is_home_rented",   limit: 4,   default: 0,   null: false
    t.float   "salary_per_month", limit: 24,  default: 0.0, null: false
    t.integer "is_bankrupt",      limit: 4,   default: 0,   null: false
    t.integer "no_of_loans",      limit: 4,   default: 0,   null: false
    t.string  "company_name",     limit: 255, default: "",  null: false
    t.string  "lic_no",           limit: 255, default: "",  null: false
    t.string  "biz_reg_no",       limit: 255, default: "",  null: false
    t.string  "day_from",         limit: 255, default: "",  null: false
    t.string  "day_to",           limit: 255, default: "",  null: false
    t.string  "time_from",        limit: 255, default: "",  null: false
    t.string  "time_to",          limit: 255, default: "",  null: false
    t.string  "address",          limit: 255, default: "",  null: false
    t.string  "country",          limit: 255, default: "",  null: false
    t.string  "zip",              limit: 255, default: "",  null: false
    t.float   "lat",              limit: 24,  default: 0.0
    t.float   "lng",              limit: 24,  default: 0.0
    t.float   "rating",           limit: 24,  default: 0.0, null: false
    t.string  "regid",            limit: 255, default: "",  null: false
    t.integer "device_type",      limit: 4,   default: 0,   null: false
    t.integer "min_loan",         limit: 4,   default: 0,   null: false
    t.integer "max_loan",         limit: 4,   default: 0,   null: false
    t.integer "status",           limit: 4,   default: 0,   null: false
    t.integer "phone_verify",     limit: 4,   default: 0
    t.string  "pin",              limit: 255, default: ""
  end

  add_foreign_key "devices", "users", primary_key: "user_id", name: "devices_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "favourites", "loan_applications", primary_key: "loan_application_id", name: "favourites_ibfk_4", on_update: :cascade, on_delete: :cascade
  add_foreign_key "favourites", "loans", primary_key: "loan_id", name: "favourites_ibfk_3", on_update: :cascade, on_delete: :cascade
  add_foreign_key "favourites", "users", column: "friend_id", primary_key: "user_id", name: "favourites_ibfk_2", on_update: :cascade, on_delete: :cascade
  add_foreign_key "favourites", "users", primary_key: "user_id", name: "favourites_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "i_friends", "loan_applications", primary_key: "loan_application_id", name: "i_friends_ibfk_4", on_update: :cascade, on_delete: :cascade
  add_foreign_key "i_friends", "loans", primary_key: "loan_id", name: "i_friends_ibfk_3", on_update: :cascade, on_delete: :cascade
  add_foreign_key "i_friends", "users", column: "friend_id", primary_key: "user_id", name: "i_friends_ibfk_2", on_update: :cascade, on_delete: :cascade
  add_foreign_key "i_friends", "users", primary_key: "user_id", name: "i_friends_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "loan_applications", "loans", primary_key: "loan_id", name: "loan_applications_ibfk_2", on_update: :cascade, on_delete: :cascade
  add_foreign_key "loan_applications", "users", column: "lender_id", primary_key: "user_id", name: "loan_applications_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "loans", "users", column: "borrower_id", primary_key: "user_id", name: "loans_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "logins", "devices", primary_key: "device_id", name: "logins_ibfk_2", on_update: :cascade, on_delete: :cascade
  add_foreign_key "logins", "users", primary_key: "user_id", name: "logins_ibfk_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "notifications", "loan_applications", primary_key: "loan_application_id", name: "notifications_ibfk_1", on_update: :cascade, on_delete: :cascade
end
