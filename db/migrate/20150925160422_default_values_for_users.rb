class DefaultValuesForUsers < ActiveRecord::Migration
  def change
    change_column :users, :is_borrower, :integer, default: 0
    change_column :users, :is_staff, :integer, :default => 0
    change_column :users,  :photo, :string, :default => ''
    change_column :users,  :name, :string, :default => ''
    change_column :users,  :nric, :string, :default => ''
    change_column :users,  :password, :string, :default => ''
    change_column :users,  :email, :string, :default => ''
    change_column :users,  :phone, :string, :default => ''
    change_column :users,  :job, :string, :default => ''
    change_column :users, :is_home_rented, :integer, :default => 0
    change_column :users,   :salary_per_month, :float, :default => 0
    change_column :users, :is_bankrupt, :integer, :default => 0
    change_column :users, :no_of_loans, :integer, :default => 0
    change_column :users,  :company_name, :string, :default => ''
    change_column :users,  :lic_no, :string, :default => ''
    change_column :users,  :biz_reg_no, :string, :default => ''
    change_column :users,  :day_from, :string, :default => ''
    change_column :users,  :day_to, :string, :default => ''
    change_column :users,  :time_from, :string, :default => ''
    change_column :users,  :time_to, :string, :default => ''
    change_column :users,  :address, :string, :default => ''
    change_column :users,  :country, :string, :default => ''
    change_column :users,  :zip, :string, :default => ''
    change_column :users,   :lat, :float, :default => 0
    change_column :users,   :lng, :float, :default => 0
    change_column :users,   :rating, :float, :default => 0
    change_column :users,  :regid, :string, :default => ''
    change_column :users, :device_type, :integer, :default => 0
    change_column :users, :min_loan, :integer, :default => 0
    change_column :users, :max_loan, :integer, :default => 0
    change_column :users, :status, :integer, :default => 0
    change_column :users, :phone_verify, :integer, :default => 0
    change_column :users,  :pin, :string, :default => ''
  end
end