class AddDefaultFieldsForApps < ActiveRecord::Migration
  def change
    change_column :loan_applications, :loan_application_status, :integer, default: 0
    change_column :loan_applications, :return_amount, :float, default: 0
    change_column :loan_applications, :per, :integer, default: 0
    change_column :loan_applications, :over_times, :integer, default: 0
    change_column :loan_applications, :sent_time, :integer, default: 0
    change_column :loan_applications, :bidded_time, :integer, default: 0
    change_column :loan_applications, :confirmed_time, :integer, default: 0

    change_column :loans, :amount, :float, default: 0
    change_column :loans, :time, :integer, default: 0
    change_column :loans, :status, :integer, default: 0
    change_column :loans, :confirmed_loan_application_id, :integer, default: 0
    change_column :loans, :aip, :string, default: 0
  end
end
