class AddPrimaryToTermsAndPolicies < ActiveRecord::Migration
  def change
    add_column :privacies, :id, :primary_key
    add_column :terms, :id, :primary_key
  end
end
