class RenameAllTables < ActiveRecord::Migration
  def change
    rename_table :admin, :admins
    rename_table :currency, :currencies
    rename_table :device, :devices
    rename_table :favourite, :favourites
    rename_table :i_friend, :i_friends
    rename_table :loan, :loans
    rename_table :loan_application, :loan_applications
    rename_table :login, :logins
    rename_table :notification, :notifications
    rename_table :privacy, :privacies
    rename_table :rate, :rates
    rename_table :setting, :settings
    rename_table :ticket, :tickets
    rename_table :ticket_message, :ticket_messages
    rename_table :user, :users
  end
end
