// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require 'libs/modernizr.custom.js'
//= require 'jRespond.min.js'
//= require 'plugins/core/slimscroll/jquery.slimscroll.min.js'
//= require 'plugins/core/slimscroll/jquery.slimscroll.horizontal.min.js'
//= require 'plugins/core/fastclick/fastclick.js'
//= require 'plugins/core/velocity/jquery.velocity.min.js'
//= require 'plugins/ui/bootbox/bootbox.js'
//= require 'plugins/charts/knob/jquery.knob.js'
//= require 'plugins/charts/flot/jquery.flot.custom.js'
//= require 'plugins/charts/flot/jquery.flot.pie.js'
//= require 'plugins/charts/flot/jquery.flot.resize.js'
//= require 'plugins/charts/flot/jquery.flot.time.js'
//= require 'plugins/charts/flot/jquery.flot.growraf.js'
//= require 'plugins/charts/flot/jquery.flot.categories.js'
//= require 'plugins/charts/flot/jquery.flot.stack.js'
//= require 'plugins/charts/flot/jquery.flot.orderBars.js'
//= require 'plugins/charts/flot/jquery.flot.tooltip.min.js'
//= require 'plugins/ui/waypoint/waypoints.js'
//= require 'plugins/forms/autosize/jquery.autosize.js'
//= require 'jquery.supr.js'
//= require 'main.js'
//= require 'plugins/forms/bootstrap-datepicker/bootstrap-datepicker.js'
//= require 'pages/custom.js'
//= require 'pages/charts-flot.js'
//= require 'plugins/tables/datatables/jquery.dataTables.js'
//= require 'plugins/tables/datatables/dataTables.tableTools.js'
//= require 'plugins/tables/datatables/dataTables.bootstrap.js'
//= require 'plugins/tables/datatables/dataTables.responsive.js'
//= require 'plugins/forms/checkall/jquery.checkAll.js'
//= require 'plugins/ui/waypoint/waypoints.js'
//= require 'pages/tables-data.js'
//= require 'pages/tables-basic.js'

