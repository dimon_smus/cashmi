// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require "plugins/core/pace/pace.min.js"
//= require 'plugins/charts/sparklines/jquery.sparkline.js'
//  Form plugins
//= require 'plugins/forms/validation/jquery.validate.js'
//= require 'plugins/forms/validation/additional-methods.min.js'
// Bootstrap plugins
//= require 'bootstrap/bootstrap'
// For ie
//= require 'libs/excanvas.min.js'
//= require 'libs/respond.min.js'
// For admin page
//= require 'admin_index'
