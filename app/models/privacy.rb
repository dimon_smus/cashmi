class Privacy < ActiveRecord::Base

  def self.get
    self.first
  end

  def self.save(message)
    self.first ? self.first.update_attributes(message) : self.create(message)
  end

end
