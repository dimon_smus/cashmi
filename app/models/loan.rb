class Loan < ActiveRecord::Base
  has_many :loan_applications
  belongs_to :user, primary_key: :user_id
  belongs_to :user, foreign_key: :borrower_id
end
