class LoanApplication < ActiveRecord::Base
  belongs_to :loan
  belongs_to :user, primary_key: :user_id
  belongs_to :user, foreign_key: :lender_id

  def self.add_active(loan_params, app_params)

    unless app_params[:loan_id]
      loan = Loan.create(loan_params)
      app_params[:loan_id] = loan.id
    end

    LoanApplication.create(app_params)
  end

  def self.edit_active(loan_params, app_params, id)
    loan_app = LoanApplication.find(id)
    loan_app.update_attributes(app_params)

    Loan.find(loan_app.loan_id).update_attributes(loan_params)
  end
end
