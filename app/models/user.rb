class User < ActiveRecord::Base

  has_many :loans, foreign_key: :borrower_id
  has_many :loan_applications, foreign_key: :lender_id
  has_many :tickets, foreign_key: :user_id

  has_many :rates, foreign_key: :from_user_id
  has_many :rates, foreign_key: :to_user_id

  def self.get_borrowers
    User.where(is_borrower: 1)
  end

  def self.get_lenders
    User.where(is_borrower: 0)
  end

end
