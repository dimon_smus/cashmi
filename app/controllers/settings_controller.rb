class SettingsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def show
    @terms = Terms.get
    @privacy = Privacy.get
  end

  def save_terms
    Terms.save terms_params params

    redirect_to settings_show_url
  end

  def save_privacy
    Privacy.save privacy_params params

    redirect_to settings_show_url
  end

  def terms_params(params)
    params.require(:settings).permit(:terms_text)
  end

  def privacy_params(params)
    params.require(:settings).permit(:privacy_text)
  end

end