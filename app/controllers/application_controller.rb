class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :check_admin
  before_action :authenticate_admin!, except: :index

  def index
    render 'admin/index'
  end

  private

  def check_admin
    redirect_to admin_session_path unless current_admin
  end
end
