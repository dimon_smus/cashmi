class LendersController < ApplicationController

  def show
    @lenders = User.get_lenders
  end

  def edit
    @lender = User.find(params[:id])
  end

  def update
    User.find(params[:user][:id]).update_attributes(update_lender_params params)

    redirect_to lenders_show_url
  end

  def new
    @lender = User.new
  end

  def delete
    User.destroy(params[:id])

    redirect_to lenders_show_url
  end

  def save
    User.create(new_lender_params params)

    redirect_to lenders_show_url
  end

  private

  def update_lender_params(params)
    params.require(:user).permit(:name, :email, :phone, :is_home_rented, :device_type,
                                 :biz_reg_no, :address,
                                 :password, :day_from, :time_from, :zip, :lic_no, :status)
  end

  def new_lender_params(params)
    params.require(:user).permit(:name, :email, :phone, :device_type, :is_home_rented, :lic_no,
                                 :biz_reg_no, :address,
                                 :day_from, :time_from, :country, :zip, :status)
  end

end