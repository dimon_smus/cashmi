class BorrowersController < ApplicationController

  def show
    @borrowers = User.get_borrowers
  end

  def edit
    @borrower = User.find(params[:id])
  end

  def update
    User.find(params[:user][:id]).update_attributes(update_borrower_params params)

    redirect_to borrowers_show_url
  end

  def new
    @borrower = User.new
  end

  def delete
    User.destroy(params[:id])

    redirect_to borrowers_show_url
  end

  def save
    User.create(save_borrower_params params)

    redirect_to borrowers_show_url
  end

  # Active borrowers section

  def active_borrowers
    @borrowers = LoanApplication.where('loan_application_status = 1')
  end

  def pending_applications
    @borrowers = LoanApplication.where('loan_application_status = 2')
  end

  def confirmed_applications
    @borrowers = LoanApplication.where('loan_application_status = 3')
  end

  # Delete active applications section

  def active_delete
    LoanApplication.destroy(params[:id])

    redirect_to root_path
  end

  # Adding and edition applications section

  def add_active
    @borrowers = User.get_borrowers
    @lenders = User.get_lenders
  end

  def save_active
    LoanApplication.add_active active_loan_params(params), active_loan_app_params(params)

    redirect_to active_borrowers_path
  end

  def edit_active
    @borrowers = User.get_borrowers
    @lenders = User.get_lenders
    @loan_app = LoanApplication.find(params[:id])
  end

  def save_edit_active
    LoanApplication.edit_active active_loan_params(params), active_loan_app_params(params), params[:loan_app][:id]

    redirect_to active_borrowers_path
  end

  def add_pending
  end

  def save_pending
    LoanApplication.add_active active_loan_params(params), pending_loan_app_params(params)

    redirect_to pending_applications_path
  end

  def edit_pending
    @loan_app = LoanApplication.find(params[:id])
  end

  def save_edit_pending
    LoanApplication.edit_active active_loan_params(params), pending_loan_app_params(params), params[:loan_app][:id]

    redirect_to pending_applications_path
  end

  def add_confirmed
  end

  def save_confirmed
    LoanApplication.add_active active_loan_params(params), pending_loan_app_params(params)

    redirect_to confirmed_applications_path
  end

  def edit_confirmed
    @loan_app = LoanApplication.find(params[:id])
  end

  def save_edit_confirmed
    LoanApplication.edit_active active_loan_params(params), pending_loan_app_params(params), params[:loan_app][:id]

    redirect_to confirmed_applications_path
  end

  private

  def update_borrower_params(params)
    params.require(:user).permit(:name, :email, :phone, :is_home_rented, :device_type,
                                 :biz_reg_no, :address, :no_of_loans, :nric,
                                 :password, :status, :is_bankrupt, :salary_per_month,
                                 :salary_per_month, :job, :is_home_rented)
  end

  def save_borrower_params(params)
    params.require(:user).permit(:name, :email, :phone, :nric, :job, :address,
                                 :device_type,:is_home_rented,
                                 :biz_reg_no, :no_of_loans,
                                 :salary_per_month, :status, :is_borrower)
  end

  def active_loan_params(params)
    params.require(:loan).permit(:borrower_id, :amount, :time)
  end

  def active_loan_app_params(params)
    params.require(:loan_app).permit(:lender_id, :loan_application_status)
  end

  def pending_loan_app_params(params)
    params.require(:loan_app).permit(:lender_id, :loan_application_status, :loan_id,
                                 :return_amount, :per, :over_times, :bidded_time)
  end

end