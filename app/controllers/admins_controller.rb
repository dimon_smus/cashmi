class AdminsController < ApplicationController

  def show
    @admins = Admin.all
  end

  def edit
    @admin = Admin.find(params[:id])
  end

  def update
    Admin.find(params[:user][:id]).update_attributes(update_admin_params params)

    redirect_to admins_show_url
  end

  def save
    Admin.create(update_admin_params params)

    redirect_to admins_show_url
  end

  def active_borrowers_list
    @borrowers = LoanApplication.all

    render 'admin/active-borowers'
  end

  def ratings
    @ratings = Rate.all

    render 'admin/user_ratings'
  end

  def chats
    @chats = IFriend.all

    render 'admin/chats'
  end

  private

  def update_admin_params(params)
    params.require(:user).permit(:email, :password)
  end

end
