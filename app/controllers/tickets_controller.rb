class TicketsController < ApplicationController

  def show
    @tickets = Ticket.all
  end

  def edit
    @ticket = Ticket.find(params[:id])
  end

  # def update
  #
  #   redirect_to lenders_show_url
  # end

  def new
    @ticket = Ticket.new
  end

  def delete
    Ticket.destroy(params[:id])

    redirect_to lenders_show_url
  end

end