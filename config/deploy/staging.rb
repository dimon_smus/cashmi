set :stage, :staging

set :deploy_to, '/home/cashme/cashme_app/'

set :branch, 'master'

set :rails_env, 'production'

# set :domain, 'testone@test.sloboda-studio.com'

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
server 'cashme.sloboda-studio.com',
       user: 'cashme',
       roles: %w(web app db),
       ssh_options: { port: 3333 }