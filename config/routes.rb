Rails.application.routes.draw do
  devise_for :admins, controllers: { sessions: 'admins/sessions' }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'application#index'

  # Lenders
  get 'lenders/:id/edit' => 'lenders#edit', as: 'lenders_edit'
  get 'lenders/:id/delete' => 'lenders#delete', as: 'lenders_delete'
  get 'lenders/' => 'lenders#show', as: 'lenders_show'
  get 'lenders_update/' => 'lenders#update', as: 'lenders_update'
  get 'lenders_new/' => 'lenders#new', as: 'lenders_new'
  get 'lenders_save/' => 'lenders#save', as: 'lenders_save'

  # Borrowers
  get 'borrowers/:id/edit' => 'borrowers#edit', as: 'borrowers_edit'
  get 'borrowers/:id/delete' => 'borrowers#delete', as: 'borrowers_delete'
  get 'borrowers/' => 'borrowers#show', as: 'borrowers_show'
  get 'borrowers_update/' => 'borrowers#update', as: 'borrowers_update'
  get 'borrowers_new/' => 'borrowers#new', as: 'borrowers_new'
  get 'borrowers_save/' => 'borrowers#save', as: 'borrowers_save'

  # Active borrowers
  get 'active_borrowers/' => 'borrowers#active_borrowers', as: 'active_borrowers'
  get 'add_active/' => 'borrowers#add_active', as: 'add_active'
  get 'save_active/' => 'borrowers#save_active', as: 'save_active'
  get 'edit_active/:id/edit' => 'borrowers#edit_active', as: 'borrowers_edit_active'
  get 'save_edit_active/' => 'borrowers#save_edit_active', as: 'save_edit_active'
  # Confirmed borrowers
  get 'confirmed_applications/' => 'borrowers#confirmed_applications', as: 'confirmed_applications'
  get 'add_confirmed/' => 'borrowers#add_confirmed', as: 'add_confirmed'
  get 'save_confirmed/' => 'borrowers#save_confirmed', as: 'save_confirmed'
  get 'edit_confirmed/:id/edit' => 'borrowers#edit_confirmed', as: 'borrowers_edit_confirmed'
  get 'save_edit_confirmed/' => 'borrowers#save_edit_confirmed', as: 'save_edit_confirmed'
  # Pending borrowers
  get 'pending_applications/' => 'borrowers#pending_applications', as: 'pending_applications'
  get 'add_pending/' => 'borrowers#add_pending', as: 'add_pending'
  get 'save_pending/' => 'borrowers#save_pending', as: 'save_pending'
  get 'edit_pending/:id/edit' => 'borrowers#edit_pending', as: 'borrowers_edit_pending'
  get 'save_edit_pending/' => 'borrowers#save_edit_pending', as: 'save_edit_pending'

  # Destroy loans

  get 'active_borrowers/:id/delete' => 'borrowers#active_delete', as: 'active_borrowers_delete'

  # Admins
  get 'admins/:id/edit' => 'admins#edit', as: 'admins_edit'
  get 'admins/:id/delete' => 'admins#delete', as: 'admins_delete'
  get 'admins/' => 'admins#show', as: 'admins_show'
  get 'admins_update/' => 'admins#update', as: 'admins_update'
  get 'admins_new/' => 'admins#new', as: 'admins_new'
  get 'admins_save/' => 'admins#save', as: 'admins_save'

  # Ticket
  get 'tickets/:id/edit' => 'tickets#edit', as: 'tickets_edit'
  get 'tickets/:id/delete' => 'tickets#delete', as: 'tickets_delete'
  get 'tickets/' => 'tickets#show', as: 'tickets_show'
  get 'tickets_update/' => 'tickets#update', as: 'tickets_update'
  get 'tickets_new/' => 'tickets#new', as: 'tickets_new'

  # Settings
  get 'settings/' => 'settings#show', as: 'settings_show'
  post 'save_terms/' => 'settings#save_terms', as: 'settings_save_terms'
  post 'save_privacy/' => 'settings#save_privacy', as: 'settings_save_privacy'

  get '/ratings', to: 'admins#ratings'
  get '/chats', to: 'admins#chats'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
